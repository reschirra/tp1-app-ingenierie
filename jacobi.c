#include "struct.h"
#include "matrix.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define ABS(x) (((x)<0) ? (-x) : (x))


rationnel* Upper(rationnel* mat, int m, int n){ // m colonne, n ligne
    rationnel* up = malloc(n*m*(sizeof(rationnel)));
    for(int i=0;i<n;++i){
        for(int j=i+1;j<m;++j){
            up[coord(m,n,j,i)] = mat[coord(m,n,j,i)];
        }
    }
    return up;
}

rationnel* Lower(rationnel* mat, int m, int n){ // m colonne, n ligne
    rationnel* low = malloc(n*m*(sizeof(rationnel)));
    for(int i=0;i<n;++i){
        for(int j=0;j<i;++j){
            low[coord(m,n,j,i)] = mat[coord(m,n,j,i)];
        }
    }
    return low;
}

rationnel* Diagonale(rationnel* mat, int m, int n){ // m colonne, n ligne (avoir matrice carrée)
    rationnel* diag = malloc(n*m*(sizeof(rationnel)));
    for(int i=0;i<n;++i){
        diag[coord(m,n,i,i)] = mat[coord(m,n,i,i)];
    }
    return diag;
}

float absolu(float a){
    return (a<0) ? -a : a;
}

float calculErreur(rationnel* matA, int m, int n,rationnel* matX, rationnel* matB, int m2){
    rationnel erreur={0,1};
    rationnel erreurMax={0,1};
    rationnel* res = malloc(n*m2*sizeof(rationnel));
    mat_mul(matA,matX,res,n,n,1,n);
    printf("----------------\nici RES \n");
    display(res,1,n);
    display(matB,1,n);
    printf("----------------\n");
    for(int i=0;i<n;++i){
        erreur = soustraire(matB[coord(1,n,0,i)],res[coord(1,n,0,i)]);
        if (absolu(erreur.a/erreur.b) > absolu(erreurMax.a/erreurMax.b)){
            printf("changement\n %f pour %f\n\n",absolu(erreurMax.a/erreurMax.b), absolu(erreur.a/erreur.b));
            erreurMax = erreur;
        }
        
    }
    float rep = absolu(erreurMax.a/erreurMax.b);
    return rep;
}

void copier(rationnel* mat, rationnel* cible,int m, int n){
    for(int i=0;i<n;++i){ // ligne
        for(int j=0;j<m;++j){ // colonne
            cible[coord(m,n,j,i)] = mat[coord(m,n,j,i)];
        }
    }
}

rationnel* resoudreJacobi(rationnel* mat, int n, rationnel* B){
    rationnel* X = malloc(n*sizeof(rationnel));
    rationnel* nextX = malloc(n*sizeof(rationnel)); // matrice verticale
    int iter=0;
    for(int i=0;i<n;++i){
        X[coord(1,n,0,i)].b = 1;
        X[coord(1,n,0,i)].a = 0;
        nextX[coord(1,n,0,i)].b = 1;
        nextX[coord(1,n,0,i)].a = 0;
    }
    float erreur = calculErreur(mat,n,n,X,B,n);
    while (erreur>0.0001 && iter<500){
        iter++;
        printf("erreur : %f\n",erreur);
        for(int i=0; i<n;++i){ // ligne
            rationnel somme = {0,1};
            rationnel current = mat[coord(n,n,i,i)];
            for(int j=0;j<n;++j){ // colonne
                if(j!=i){
                    somme = addition(somme,multiplier(mat[coord(n,n,j,i)],X[coord(1,n,0,j)]));
                }
            }
            somme = soustraire(B[coord(1,n,0,i)],somme);
            somme = diviser(somme,current);
            nextX[coord(1,n,0,i)] = somme;
        }
        copier(nextX,X,1,n);
        erreur = calculErreur(mat,n,n,X,B,n);
    }
    printf("erreur : %f\n",erreur);
    return X;
}

rationnel* seidel_rationnel(rationnel* A, int n, rationnel* B) {
    rationnel *x = calloc(n, sizeof(rationnel));
    rationnel *x_prev = calloc(n, sizeof(rationnel));

    for (int i = 0; i < n; i++) {
        x[i].a = 0; x[i].b = 1;  
        x_prev[i].a = 0; x_prev[i].b = 1;
    }

    int iter = 0;
    const int max_iter = 500;
    rationnel tol = {1, 10000}; 

    rationnel err;
    do {
        for (int i = 0; i < n; i++) {
            x_prev[i] = x[i];
        }

        for (int i = 0; i < n; i++) {
            rationnel sum = {0, 1};  

            for (int j = 0; j < n; j++) {
                if (j != i) {
                    sum = addition(sum, soustraire(A[i * n + j], x[j]));
                }
            }

            rationnel numerateur = soustraire(B[i], sum);
            x[i] = diviser(numerateur, A[i * n + i]);  
        }

        err.a = 0; err.b = 1;  
        for (int i = 0; i < n; i++) {
            rationnel diff = soustraire(x[i], x_prev[i]);
            if (ABS(diff.a * err.b) > ABS(diff.b * err.a)) {
                err = diff;  
            }
        }

        iter++;
    } while (ABS(err.a * tol.b) > ABS(tol.a * err.b) && iter < max_iter);

    free(x_prev);

    return x;
}