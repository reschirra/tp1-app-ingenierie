#include <stdlib.h>
#include <stdio.h>
#include "struct.h"

float* new_matrix(int n) {
    float* m = malloc(n * n * sizeof(float));
    if (m == NULL) {
        exit(1);
    }
    return m;
}

int coord_square(int n, int x, int y) {
    return y * n + x;
}

int coord(int m, int n, int x, int y) {
    return m*y + x;
}

void display(rationnel* mat, int m, int n) {
    for (int i=0; i<n; ++i) {
        for (int j=0; j<m; ++j) {
            printf("%d/%d ", mat[coord(m, n, j, i)].a,mat[coord(m, n, j, i)].b);
        }
        printf("\n");
    }
    printf("\n");
}



void mat_mul(rationnel* a, rationnel* b, rationnel* res, int m1, int n1, int m2, int n2) {
    if (m1 != n2) exit(2);
    int res_m = m2, res_n = n1;
    rationnel temp;
    for (int i = 0; i < res_n; i++) // ligne 
    {
        for (int j = 0; j < res_m; j++) // colonne
        {
            rationnel s = {0,1};
            for (int k = 0; k < m1; k++)
            {
                temp = multiplier(a[coord(m1,n1,k,i)],b[coord(m2,n2,k,j)]);
                s = addition(s,temp);
            }
            res[coord(res_m,res_n,j,i)] = s;
        }
    }
}

void mat_mul_square(float* a, float* b, float* res, int n) {
    for (int y = 0; y < n; y++)
    {
        for (int x = 0; x < n; x++)
        {
            float s = 0; // initialisation de la somme
            for (int i = 0; i < n; i++)
            {
                s += a[coord_square(n, i, y)] * b[coord_square(n, x, i)];
            }
            res[coord_square(n, x, y)] = s;
            
        }
        
    }
    
}