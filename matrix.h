#include "struct.h"
#ifndef MATRIX_H
#define MATRIX_H

int coord_square(int n, int x, int y);
int coord(int m, int n, int x, int y);
void display(rationnel* mat, int m, int n);
float* new_matrix(int n);
void mat_mul_square(float* a, float* b, float* res, int n);
void mat_mul(rationnel* a, rationnel* b, rationnel* res, int m1, int n1, int m2, int n2);

#endif