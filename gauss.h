#include "struct.h"
#ifndef GAUSS_H
#define GAUSS_H

void echelonner(rationnel* mat, int n, int m);
void normalise(rationnel* mat, int n, int m);
void reduire(rationnel* mat, int n, int m);
rationnel* augmenter(rationnel* mat, int n, int m, rationnel *mat2, int m2);
rationnel* separer(rationnel* mat, int n, int m, int col);
rationnel* resoudreGauss(rationnel *mat, int n, int m, rationnel* B, int m2);

#endif