#ifndef STRUCT_H
#define STRUCT_H

typedef struct rationnel{
    int a;
    int b;
}rationnel;

int pgcd(int a, int b);
rationnel reduction(rationnel q);
rationnel addition(rationnel q1, rationnel q2);
rationnel multiplier(rationnel q1, rationnel q2);
rationnel diviser(rationnel q1, rationnel q2);
rationnel soustraire(rationnel q1, rationnel q2);

#endif