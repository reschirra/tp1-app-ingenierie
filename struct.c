#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct rationnel{
    int a;
    int b;
};
typedef struct rationnel rationnel;

int pgcd(int a, int b){
    if(a==0) return 1;
    a = abs(a);
    b = abs(b);
    int c = 0;
    while (a!=b){
        if (b>a){
        c = a;
        a = b;
        b = c;
        }
        a = a - b;
    }
    if(a==0) return 1;
    return a;
}

rationnel reduction(rationnel q){
    int dem = pgcd(q.a,q.b);
    rationnel reduit = {q.a/dem,q.b/dem};
    return reduit;
}

rationnel addition(rationnel q1, rationnel q2){
    rationnel ajouter;
    ajouter.b = q2.b*q1.b;
    ajouter.a = q1.a*q2.b + q2.a*q1.b;
    ajouter = reduction(ajouter);
    return ajouter;
}

rationnel soustraire(rationnel q1, rationnel q2){
    rationnel sous;
    sous.b = q2.b*q1.b;
    sous.a = q1.a*q2.b - q2.a*q1.b;
    sous = reduction(sous);
    return sous;
}

rationnel multiplier(rationnel q1, rationnel q2){
    rationnel mult;
    mult.b = q2.b*q1.b;
    mult.a = q1.a*q2.a;
    mult = reduction(mult);
    return mult;
}

rationnel diviser(rationnel q1, rationnel q2){
    rationnel div;
    div.b = q2.a*q1.b;
    div.a = q1.a*q2.b;
    div = reduction(div);
    return div;
}