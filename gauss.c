#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "matrix.h"
#include "gauss.h"
#include "struct.h"
/*
*  entrée : matrice mat avec m-colonnes et n-lignes
*  la fonction transforme la matrice mat en une matrice triangulaire supérieur
*  en utilisant les opérations sur les lignes.
*/
void echelonner(rationnel* mat, int n, int m){
    rationnel pivot = {0,1};
    rationnel coef = {0,1};
    rationnel temp;
    for(int i=0;i<n-1;++i){
        pivot = mat[coord(m,n,i,i)]; 
        for(int j=i+1;j<n;++j){
            coef = diviser(mat[coord(m,n,i,j)],pivot); 
            for(int k=i;k<m;++k){
                temp = multiplier(mat[coord(m,n,k,i)],coef);
                mat[coord(m,n,k,j)] = soustraire(mat[coord(m,n,k,j)],temp);
            }
        }
    }
}

/*
*  entrée : matrice mat avec m-colonnes et n-lignes
*  la fonction met tout les éléments sur la diagonale à '1' 
*/
void normalise(rationnel* mat, int n, int m){
    rationnel ref;
    for(int i=0;i<n;++i){
        ref = mat[coord(m,n,i,i)];
        for(int k=i;k<m;++k){
            mat[coord(m,n,k,i)] = diviser(mat[coord(m,n,k,i)],ref);
        }
    }
}

/*
* entrée : matrice mat avec m-colonnes et n-lignes
* la fonction transforme la matrice en une matrice identitée en utilisant le même procéder
* que pour la transformer en matrice triangulaire supérieur
*/
void reduire(rationnel* mat, int n, int m){
    rationnel ref;
    rationnel temp;
    for(int i=n-1; i>=0; --i){
        for(int j=i-1;j>=0;--j) {
            ref = mat[coord(m,n,i,j)];
            for(int k=i; k<m; ++k){ 
                temp = multiplier(ref,mat[coord(m,n,k,i)]);
                mat[coord(m,n,k,j)] = soustraire(mat[coord(m,n,k,j)],temp);
            }
        }
    }
}

/*
* entrée : deux matrices mat avec m-colonnes et n-lignes
* la fonction augmente la première matrice avec la seconde, transformant le tout 
* en une matrice à m+m2-colonnes et n-lignes;
*/
rationnel* augmenter(rationnel* mat, int n, int m, rationnel* mat2, int m2){
    rationnel* matAugm = malloc(n*(m+m2)*sizeof(rationnel));
    for(int i=0; i<n; ++i){ // ligne
        for(int j=0; j<m+m2; ++j){ // colonne
            if (j<m) matAugm[coord(m+m2,n,j,i)] = mat[coord(m,n,j,i)];
            else matAugm[coord(m+m2,n,j,i)] = mat2[coord(m2,n,j-m,i)];
        }
    }
    return matAugm;
}

/*
* entrée : une matrice mat avec m-colonnes et n-lignes et col, le nombre de colonnes d'une seconde matrice
* la fonction sépare une matrice et ne renvois que la dernière partie de celle-ci, la dernière partie aura col-colonnes
* sortie : une matrice avec col-colonnes et n-lignes
*/
rationnel* separer(rationnel* mat, int n, int m, int col){ 
    rationnel* matSep = malloc(n*col*sizeof(rationnel));
    for(int i=0; i<n; ++i){
        for(int j=m-col; j<m; ++j){
            matSep[coord(col,n,j-(m-col),i)] = mat[coord(m,n,j,i)];
        }
    }
    return matSep;
}

/*
* entrée : une matrice mat avec m-colonnes et n-lignes et B une seconde matrice
* la fonction trouve le 'x' dans mat*x = B et le renvois
* sortie : une matrice avec col-colonnes et n-lignes
*/
rationnel* resoudreGauss(rationnel *mat, int n, int m, rationnel* B, int m2){
    rationnel* rep = malloc(m2*n*sizeof(rationnel));
    rationnel* augm = augmenter(mat,n,m,B,m2);
    echelonner(augm,n,m+m2);
    normalise(augm,n,m+m2);
    reduire(augm,n,n+m2);
    rep = separer(augm,n,m+m2,m2);
    return rep;
}