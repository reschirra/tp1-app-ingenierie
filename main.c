#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "gauss.h"
#include "jacobi.h"
#include "matrix.h"
#include "struct.h"

void matA6(rationnel* mat, int n){
    for(int i=0; i<n; ++i){ // ligne
        for(int j=0; j<n; ++j){ // colonne
            if (i==j) {
                mat[coord(n,n,i,i)].a = 3;
                mat[coord(n,n,i,i)].b = 1;
            }
            else if (j == i+1) {
                if(i<n){
                    mat[coord(n,n,j,i)].a = -1;
                    mat[coord(n,n,j,i)].b = 1;
                }
            }
            else if (j == i-1){
                if(i>0){
                    mat[coord(n,n,j,i)].a = -2;
                    mat[coord(n,n,j,i)].b = 1;
                }
            }
            else {
                mat[coord(n,n,j,i)].a = 0;
                mat[coord(n,n,j,i)].b = 1;
            }
            
        }
    }
}

void matA5(rationnel* mat, int n) {
    int temp;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            rationnel v;
            if (i == j) {
                v.a = 1;
                v.b = 1;
            } else if (i == 0 || j == 0) {
                temp = pow(2, abs(-j));
                if (1-j-1 < 0){
                    v.a = 1;
                    v.b = temp;
                } else {
                    v.a = temp;
                    v.b = 1;
                }
            } else {
                v.a = 0;
                v.b = 1;
            }
            mat[i*n+j] = v;
        }
    }
}

int main(int argc, char const *argv[])
{
    rationnel A1[9] = {{3,1}, {0,1}, {4,1},
                      {7,1}, {4,1}, {2,1},
                      {-1,1}, {1,1}, {2,1}};
    rationnel A2[9] = {{-3,1}, {3,1}, {-6,1},
                      {-4,1}, {7,1}, {8,1},
                      {5,1}, {7,1}, {-9,1}};
    rationnel A3[9] = {{4,1}, {1,1}, {1,1},
                      {2,1}, {-9,1}, {0,1},
                      {0,1}, {-8,1}, {6,1}};
    rationnel A4[9] = {{7,1}, {6,1}, {9,1},
                      {4,1}, {5,1}, {-4,1},
                      {-7,1}, {-3,1}, {8,1}};

    /*display(A1,3,3);
    display(A2,3,3);
    display(A3,3,3);
    display(A4,3,3);*/

    rationnel X[3] = {{1,1},{1,1},{1,1}};
    rationnel B[3];
    mat_mul(A3,X,B,3,3,1,3);
    rationnel* solution = malloc(3*sizeof(rationnel));
    //solution = resoudreJacobi(A1,3,B);
    solution = seidel_rationnel(A3,3, B);
    display(solution,1,3);
    display(X,1,3);
    
    /*int n = 6;
    rationnel* m = malloc(n*n*sizeof(rationnel));
    rationnel* m2 = malloc(n*n*sizeof(rationnel));
    matA5(m, n);
    matA6(m2,n);
    display(m, n, n);
    display(m2, n, n);
    free(m);
    free(m2);*/
    
    return 0;
}
