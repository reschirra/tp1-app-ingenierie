#include "struct.h"
#ifndef JACOBI_H
#define JACOBI_H

rationnel* Upper(rationnel* mat, int m, int n);
rationnel* Lower(rationnel* mat, int m, int n);
rationnel* Diagonale(rationnel* mat, int m, int n);

float calculErreur(rationnel* matA, int m, int n,rationnel* matX, rationnel* matB, int m2);
void copier(rationnel* mat, rationnel* cible,int m, int n);

rationnel* resoudreJacobi(rationnel* mat, int n, rationnel* B);
rationnel* seidel_float(rationnel* mat, int n, rationnel* B);
rationnel* seidel_rationnel(rationnel* A, int n, rationnel* B);

#endif