#include <stdio.h>
#include <stdlib.h>
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define ABS(x) (((x)<0) ? (-x) : (x))

void display(float *matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%f ", matrix[i * cols + j]);  // Accès à l'élément (i, j)
        }
        printf("\n");
    }
}

int coord(int m, int n, int x, int y) {
    return m*y + x;
}

void mat_mul(float *A, float *B, float *C, int m, int n, int p) {
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < p; j++) {
            C[i * p + j] = 0.0f;
        }
    }
    
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < p; j++) {
            for (int k = 0; k < n; k++) {
                C[i * p + j] += A[i * n + k] * B[k * p + j];
            }
        }
    }
}

void copier(float* A, float* B, int m, int n){
    for(int i=0;i<n;++i){
        for(int j=0;j<m;++j){
            B[coord(m,n,i,j)] = A[coord(m,n,i,j)];
        }
    }
}

float error(float* matA, int n,float* matX, float* matB, int iter){
    float erreur = 0;
    float erreurMax = 0;
    float* res = malloc(n*sizeof(float));
    mat_mul(matA,matX,res,n,n,1);
    for(int i=0;i<n;++i){
        erreur = matB[i]-res[i];
        if (erreur < 0) {
            erreur = -erreur;
        }
        if (erreur>erreurMax) erreurMax = erreur;
    }
    return erreurMax;
}

float* seidel(float* A, int n, float* B) {
    float *x = calloc(n, sizeof(float));  
    float *x_prev = calloc(n, sizeof(float));  
    float err;
    int iter = 0;
    const float tol = 0.0001;  
    const int max_iter = 500;  

    do {
        for (int i = 0; i < n; i++) {
            x_prev[i] = x[i];
        }

        for (int i = 0; i < n; i++) {
            float sum = 0.0f;

            for (int j = 0; j < n; j++) {
                if (j != i) {
                    sum += A[i * n + j] * x[j];  
                }
            }

            x[i] = (B[i] - sum) / A[i * n + i];
        }

        err = error(A, n, x, B,iter);
        iter++;
    } while (err > tol && iter < max_iter);
    printf("Gauss-Seidel : %f | %d\n",err, iter);
    free(x_prev);

    return x;
}

float* jacobi(float* mat, int n, float* B){
    float *x = calloc(n, sizeof(float));  
    float *x_next = calloc(n, sizeof(float));  
    float err;
    int iter = 0;
    const float tol = 0.0001;  
    const int max_iter = 500;
    
    float erreur = error(mat,n,x,B,iter);
    while (erreur>tol && iter<max_iter){
        for(int i=0; i<n;++i){ // ligne
            float somme = 0;
            float current = mat[coord(n,n,i,i)];
            for(int j=0;j<n;++j){ // colonne
                if(j!=i){
                    somme += mat[coord(n,n,j,i)]*x[coord(1,n,0,j)];
                }
            }
            somme = B[coord(1,n,0,i)]-somme;
            somme = somme/current;
            x_next[coord(1,n,0,i)] = somme;
        }
        copier(x_next,x,1,n); // à faire
        iter++;
        erreur = error(mat,n,x,B,iter);
    }
    printf("Jacobi : %f | %d\n",erreur, iter);
    return x;
}


int main() {
    float A1[9] = {3,0, 4, 7, 4, 2, -1, 1, 2};
    float A2[9] = {-3, 3, -6, -4, 7, 8, 5, 7, -9};
    float A3[9] = {4, 1, 1, 2, -9, 0, 0, -8, 6};
    float A4[9] = {7,6,9,4,5,-4,-7,-3,8};
    float X[3] = {1, 1, 1};
    float B[3];

    mat_mul(A3,X,B,3,3,1);

    float* res = malloc(3*sizeof(float));
    float* res2 = malloc(3*sizeof(float));
    printf("Mat | Algo : Erreur | Iter\n");
    printf("--------------------------------------------------\n");
    printf("A1 | ");
    res = seidel(A1, 3, B);
    printf("A1 | ");
    res2 = jacobi(A1,3,B);
    printf("--------------------------------------------------\n");
    printf("A2 | ");
    res = seidel(A2, 3, B);
    printf("A2 | ");
    res2 = jacobi(A2,3,B);
    printf("--------------------------------------------------\n");
    printf("A3 | ");
    res = seidel(A3, 3, B);
    printf("A3 | ");
    res2 = jacobi(A3,3,B);
    printf("--------------------------------------------------\n");
    printf("A4 | ");
    res = seidel(A4, 3, B);
    printf("A4 | ");
    res2 = jacobi(A4,3,B);
    //display(res, 3, 1);
    //display(res2, 3, 1);

}